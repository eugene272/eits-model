#include "inc/typedef.h"
#include "inc/macro.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <stdarg.h>

#define REG_LIM 10
#define TREG_LIM 10
#define TIME 0.0

//#define USE_COULOMB
#define NORMAL_CONDITION (cell_t) {.Tl = 300, .Te=300, .U = 1.19e-9, .N=1e-9}  

static int latsize = 0;
static double* lattice = 0;

// preserves "r"
void assignCell(cell_t* to, cell_t source){
    double r = to->r;
    *to = source;
    to->r = r;
}

problem_t* initBorder(problem_t* prb,  char type, ... ){
    // Neumann
    // o -:---- x ----- o ---- .... ---- o ----:- x
    // cloffset = 0
    // croffset = -1
    // nloffset = 1
    // nroffset = 0
    
    // Dirichlet
    // o -:---- x ----- o ---- .... --:- o ------ x
    // cloffset = 0
    // croffset = -1
    // nloffset = 1
    // nroffset = -1

    va_list ap;
    va_start(ap, type);

    int size = prb->init.rsize;
    double r;

    // neumann
    if(type=='n'){
        cell_t border_cell = va_arg( ap, cell_t );
        assignCell(&prb->init.cells[size-1], border_cell);

        prb->init.cl = 0;
        prb->init.cr = -1;

        prb->init.nl = 1;
        prb->init.nr = 0;
    }
        // dirichlet
    else if(type=='d'){
        node_t border_node = va_arg( ap, node_t );
        r = prb->init.nodes[size-1].r;
        prb->init.nodes[size-1] = border_node;
        prb->init.nodes[size-1].r = r;

        prb->init.cl = 0;
        prb->init.cr = -1;

        prb->init.nl = 1;
        prb->init.nr = -1;
    }
    else{
        printf("initBorder(): Error: unknown border condition: %c", type );
        exit(1);
    }

    prb->bc = type;
    return prb;
}

problem_t* initCells( problem_t* prb, char src, ... ){
    va_list ap;
    va_start(ap,src);

    int i;

    state_t
            *init = &prb->init;
    double r;
  
    cell_t
            init_cell,
            *cells = init->cells;
    void (*fun)(cell_t* in) = 0;

    if(src == 'c'){
        init_cell = va_arg(ap,cell_t);
        FOR_CELLS(init) assignCell(&cells[i], init_cell);
    }
    else if(src=='f'){
        fun = va_arg(ap,void*);
        FOR_CELLS(init) fun( &cells[i] );
    }
    else {
        printf("initCells(): Error: unknown source: %c", src);
        exit(1);
    }
    return prb;
}

problem_t* initLattice(problem_t* prb, int size, double (*generator)(int i)){
    prb->init.rsize = size;
    prb->init.nodes = (node_t*) calloc( size, sizeof(node_t));
    prb->init.cells = (cell_t*) calloc( size, sizeof(cell_t));

    int i;
    node_t *n = prb->init.nodes;
    cell_t *c = prb->init.cells;

    n++->r = generator(0);
    for(i=1;i<size;i++){
        n++->r = generator(i);
        c++->r = (n->r + (n-1)->r)/2;
    }
    c->r = n->r;

    return prb;
}
  
  
  


problem_t* init_rlat(problem_t *prb ,char* src, char csep, char rsep){
    char* buf = calloc(strlen(src),sizeof(char));
    char *p, *pb, pattern[20];
    float dr[REG_LIM],rup[REG_LIM];
         
    float r = 0;
    int ri=0, rlen=1, h;
         
    sprintf(pattern,"%%f%c%%f",csep);
    p=&src[0];
	 
    do{
        for(pb = &buf[0];*p != rsep && *p!='\0'; p++, pb++) *pb = *p;
        *pb='\0';
        if(*p != '\0') p++;
                  
        if(sscanf(buf,pattern,&dr[ri],&rup[ri])!=2) continue;
        if( dr[ri] <= 0 || rup[ri] - r < dr[ri] ) continue;
                  
        h = (int)((rup[ri]-r)/dr[ri]);
        h = (h*dr[ri] == rup[ri] - r)? h-1 : h;
        rlen += h;
        r += dr[ri]*h;
        ri++;
    }while(*p!='\0');
         
    prb->init.rsize = rlen;
    prb->init.nodes = (node_t*) calloc( rlen, sizeof(node_t));
    prb->init.cells = (cell_t*) calloc( rlen, sizeof(cell_t));
         
    int i,k;
    node_t* pn = &prb->init.nodes[1];
    cell_t* pc = prb->init.cells;
         
    for(i=0,k=0;i<rlen-1;i++,pn++,pc++){
        if( (pn-1)->r + dr[k] >= (double) rup[k] ) k++;
        pn->r = (pn-1)->r + (double) dr[k];
        pc->r = (pn->r + (pn-1)->r)/2;
    }

    prb->init.cells[rlen-1].r = prb->init.nodes[rlen-1].r;
         
    return prb;
}

