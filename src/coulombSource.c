#include <coulomb.h>
#include <math.h>
#include <assert.h>
#include "inc/typedef.h"
#include "inc/coulombSource.h"

#define KE 5.527e-2 // e V-1 nm-1

static double v = 0.0, trel = 0.0, tscreen = 0.0;

static int initialized = 0;

static double rhoM = 0.0, fieldM = 0.0, wM = 0.0;

static void _coulombInit(){
    rhoM = rhoMax(v);
    fieldM = Field(rhoM, 3.0, v);
    wM = energyDensity(fieldM);
}

void initialize(double _v, double _trel, double _tscreen){
    assert(_v > 0.0 & _v < 1.0);
    assert(_trel > 0);
    assert(_tscreen >= 0);

    v = _v;
    trel = _trel;
    tscreen = _tscreen;

    _coulombInit();
    initialized = 1;
}

double energyDensity(double field){
    return 0.5*KE*field*field;
}

static double _eR(double rho){
    assert(initialized);
    return exp(0.5)*wM*(rho/rhoM)*exp(-0.5*pow(rho/rhoM,2.0));
}

static double _eRT(double rho, double t){
    assert(initialized);
    return _eR(rho)*0.5*exp(-t/trel - 2*t/tscreen);
}

double phCouSource( double t, cell_t cell){
    if (t > 5*tscreen){
        return 0.0;
    }
    return _eRT(cell.r, t)/trel;
}