
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "inc/macro.h"
#include "inc/params.h"
#include "inc/typedef.h"
#include "inc/coulombSource.h"

#ifdef USE_COULOMB

#include <coulomb.h>

#endif

#define NORMAL_CONDITION (cell_t) {.Tl = 300, .Te=300, .U = 1.245e-9, .N=1e-9}  

extern problem_t* init_rlat(problem_t* prb ,char* src, char csep, char rsep);
extern void init_cells();

extern void dropAt( int times, ... );
extern void drop(state_t);
extern void out(state_t);
extern void out_close();
extern char* out_header, *out_footer, *out_fname;
extern double out_r;

problem_t* initBorder( problem_t* prb, char type, ... );
problem_t* initCells(problem_t* prb, char src, ... );

void iterate(state_t* old, state_t* new, double dt);
extern cell_t citer(node_t ln, cell_t oc, node_t rn, double t, double dt);
extern void niter( node_t* n, cell_t lc, cell_t rc );

double v = 0.1;


extern double phSe;
extern double phZ, phN;

void sim(problem_t, void (*fun)(state_t));

int main(int argc, char** argv){
    switch(argc){
        case 4: out_r = atof(argv[3]);
        case 3: out_fname = argv[2];
#ifdef USE_COULOMB
        case 2: v = atof(argv[1]);
#else
            case 2: phSe=atof(argv[1]);
#endif
        case 1: ;
        default: ;
    }

#ifdef USE_COULOMB
    setTarget(phZ, phN, 8.0);
    setProjectile(26, 50.0e9);
    initialize(v, 78.0, 10.0);
    phSe = energyLossesPDU(v);
#endif

    problem_t prb;
    prb.init.t = 0;
    init_rlat(&prb,"0.8:20|1.0:100|2:1000",':','|');
    // init neumann border condition
    initBorder( &prb, 'n', NORMAL_CONDITION );
    // init cells
    initCells( &prb, 'c', NORMAL_CONDITION );
	
    drop( prb.init );

//init_cells();

    char header[100],footer[]= "";
    sprintf(header,"#v = %f\n#Se = %.1f",v,phSe);
    out_header = header;
    out_footer = footer;

    prb.tlsize = 4;
    prb.tlat = calloc(prb.tlsize, sizeof(tlat_t));
    prb.tlat[0] = (tlat_t) {.dt = 0.0002, .tup=100.0};
    prb.tlat[1] = (tlat_t) {.dt = 0.0004, .tup=500.0};
    prb.tlat[2] = (tlat_t) {.dt = 0.001, .tup=2000.0};
    prb.tlat[3] = (tlat_t) {.dt = 0.002, .tup=10000.0};

    dropAt(19,
           1.0, 2.0, 4.0, 8.0, 16.0, 50.0, 100.0, 200.0, 300.0, 400.0, 500.0,
           1000.0, 2000.0, 3000.0, 4000.0, 5000.0, 6000.0, 7000.0, 8000.0, 9000.0
    );
    sim(prb, &out);
    out_close();

    return 0;
}


void cell_iter(state_t*,state_t*,double,cell_t);
void node_iter(state_t*,node_t);

void sim(problem_t prb, void (*out)(state_t)){
    state_t st_1, st_2;
         
    st_1 = st_2 = prb.init;
         
    st_1.nodes = (node_t*) memmove(calloc(st_1.rsize,sizeof(node_t)),prb.init.nodes,st_1.rsize*sizeof(node_t));
    assert(memcmp(st_1.nodes, prb.init.nodes, st_1.rsize*sizeof(node_t)) == 0);

    st_1.cells = (cell_t*) memmove(calloc(st_1.rsize,sizeof(cell_t)),prb.init.cells,st_1.rsize*sizeof(cell_t));
    assert(memcmp(st_1.cells, prb.init.cells, st_1.rsize*sizeof(cell_t)) == 0);
         
    st_2.nodes = (node_t*) memmove(calloc(st_2.rsize,sizeof(node_t)),prb.init.nodes,st_2.rsize*sizeof(node_t));
    st_2.cells = (cell_t*) memmove(calloc(st_1.rsize,sizeof(cell_t)),prb.init.cells,st_1.rsize*sizeof(cell_t));
    assert(memcmp(st_1.nodes, st_2.nodes, st_1.rsize*sizeof(node_t)) == 0);
    
    state_t  *old = &st_1,
            *new = &st_2,
            *tmp;
          
    int i;
    double dt, tup;
    /*
    // kostyl
    // ===================================================
    node_t
        *nl = st_1.nodes,
        *nr = nl+1;
    
    for( i=0; i < old->rsize-1; i++ ){
        old->cells[i].r = new->cells[i].r = ( nl[i].r + nr[i].r )/2;
    }
    // i = rsize-1
    old->cells[i].r = new->cells[i].r = nl[i].r;
    // ====================================================
    */    
    for(i=0; i<prb.tlsize; i++){
        dt = prb.tlat[i].dt;
        tup = prb.tlat[i].tup;
        assert( dt > 0 && tup > dt );
                  
        do{
            iterate(old,new,dt);
            out(*new);
                           
            tmp = old;
            old = new;
            new = tmp;
        }while(old->t < tup);
    }

}

void dump(cell_t cell){
    printf("\n ===: r = %f :===\n\t U  = %e \n\t Tl = %e \n\t N  = %e \n\t Te = %e \n ======================== \n ",
           cell.r,
           cell.U,
           cell.Tl,
           cell.N,
           cell.Te
    );
}

void iterate(state_t* old, state_t* new, double dt){
    double
            t = old->t;
    
    new->t = old->t + dt;
    
    cell_t
            *oc = old->cells,
            *nc = new->cells,
    
            *lc = oc,
            *rc = lc+1;
    node_t
            *n  = old->nodes,
            *ln = n,
            *rn = ln + 1;
    


    FOR_NODES(old) niter( &n[i], lc[i-1], rc[i-1] );

    FOR_CELLS(old) nc[i] = citer( ln[i], oc[i], rn[i], t, dt );
}



