#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "inc/typedef.h"

extern double phTm;

static char empty[] = "", def_fname[]="out.dat";
char* out_fname =  def_fname;
char* out_header = empty;
char* out_footer = empty;

double out_r = 0.0, out_radius=0.0;

static FILE* fp = 0;

int skip(double t){
	if(t<1) return 40;
	else if(t<10) return 100;
	else if(t<100) return 200;
	else if(t<1000) return 500;
	else return 5000;
}


char drop_prefix[] = "drop";
void drop( state_t in ){
  char buf[100], fname[200];

  sprintf(buf,"%f.dat",in.t);
  strcpy( fname, drop_prefix );
  strcat( fname, buf );

  FILE* fp = fopen( fname, "w" );
  cell_t *c = in.cells;

  int i;

  fprintf( fp, "r; Tl Te N");
  for( i=0; i<in.rsize; i++, c++)
    fprintf( fp, "%f; %f %f %f\n", c->r, c->Tl, c->Te, c->N );

  fclose(fp);

}

double* drop_times = 0;
int drop_times_size = 0;

void dropAt(int times, ... ){
  va_list ap;
  va_start(ap, times);

  static double* ptr = 0;

  drop_times = (double*) realloc(drop_times , sizeof(double)*(drop_times_size + times)  );
  if(!drop_times_size) ptr = drop_times;

  drop_times_size += times;
  
  int i;
  for(i=0; i<times; i++, ptr++) *ptr = va_arg(ap,double);

  // printf("dropAt:\t");
  //for(i = 0; i < drop_times_size; i++) printf("%f\t", drop_times[i] );
  return;
}

void out(state_t in){
	static int counter = 0, ri=0;
	int k;
	
	if(!fp){
		fp = fopen(out_fname,"w");
		fputs(out_header,fp);
		fputc('\n',fp);
		putchar('\a');
		while(in.cells[ri].r < out_r && ri < in.rsize ) ri++;
		fprintf(fp,"# r = %.4f \n",in.cells[ri].r);
		
	}
	
	if(0==counter%skip(in.t)){
		for(k=0; in.cells[k].Tl > phTm && k < in.rsize -1; k++);
		out_radius = (in.nodes[k].r > out_radius)? in.nodes[k].r : out_radius;
		 
		if (0 == counter%10*skip(in.t)) printf("t=%.5f, rad = %f\n",in.t, out_radius);
		fprintf(fp,"%.5f\t%.4e\t%.4e\t%.4e\t%.4e\t%.4e ;",in.t,in.cells[ri].Tl,in.cells[ri].Te,in.cells[ri].N,in.cells[ri].U, in.cells[ri].Hm);
        fprintf(fp,"%.4e\t%.4e\t%.4e\n",in.nodes[ri+1].J,in.nodes[ri+1].W,in.nodes[ri+1].K);
		fflush(fp);
	}
	counter++;

	static int drop_i = 0;
	if( drop_i < drop_times_size && in.t >= drop_times[drop_i]){
	  printf("drop!");
	  drop(in);
	  drop_i++;
	} 
}



void out_close(){fprintf(fp,"%s\n#radius = %.3f nm",out_footer,out_radius); fclose(fp); fp=0; }


