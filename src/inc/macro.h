//
// Created by eugene on 5/8/17.
//

#ifndef EITS_MODEL_MACRO_H
#define EITS_MODEL_MACRO_H

// Neumann
// o -:---- x ----- o ---- .... ---- o ----:- x
// cloffset = 0
// croffset = -1
// nloffset = 1
// nroffset = 0

// Dirichlet
// o -:---- x ----- o ---- .... --:- o ------ x
// cloffset = 0
// croffset = -1
// nloffset = 1
// nroffset = -1

#define FOR_NODES(X) for(int i = X->nl; i < X->rsize + X->nr; i++)
#define FOR_CELLS(X) for(int i = X->cl; i < X->rsize + X->cr; i++)

#endif //EITS_MODEL_MACRO_H
