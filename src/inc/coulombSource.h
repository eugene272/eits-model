//
// Created by eugene on 5/8/17.
//
#ifndef TYPEDEF
#include "typedef.h"
#endif

#ifndef EITS_MODEL_COULOMBSOURCE_H
#define EITS_MODEL_COULOMBSOURCE_H
void initialize(double _v, double _trel, double _tscreen);
double energyDensity(double field);
double phCouSource( double t, cell_t cell);

#endif //EITS_MODEL_COULOMBSOURCE_H
