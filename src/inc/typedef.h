#define TYPEDEF

typedef struct {
	double r;
	double Tl, N, U, Te;
	double Hm, Hv;
} cell_t;

typedef struct {
	double r;
	double J, W, K;
} node_t;

typedef struct {
	double t;
	int rsize;
    
    int cr, cl;
    int nr, nl;
	cell_t* cells;
	node_t* nodes;
} state_t;

typedef struct {
	double dt, tup;
} tlat_t;

typedef struct {
	state_t init;
	
	int tlsize;
	tlat_t* tlat;

	char bc;
} problem_t;  





