#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "inc/params.h"
#include "inc/typedef.h"
#include "inc/ph_constants.h"
#include "inc/coulombSource.h"
    
    
#define IS_REAL(x) (!isnan((x)) && !isinf((x)))
#define IS_NAT(x)  ((x) >= 0 && !isinf((x))) 

#define FAV(f) (f(lc)*hr + f(rc)*hl)/h
#define CAV(f) ( lc.f*hr + rc.f*hl )/h

#define DF(f) ( f(rc) - f(lc) )/h
#define DC(f) ( rc.f - lc.f )/h

#ifdef USE_COULOMB
#define LATTICE_SOURCE phCouSource(t, oc)
#else
#define LATTICE_SOURCE 0.0
#endif
    
extern double phGe(double, cell_t);
extern double phUs(double, cell_t);
extern double phUl(cell_t);
extern double phCl(cell_t);
extern double phEg(cell_t);
extern double phDf(cell_t);
extern double phKe(cell_t);
extern double phKl(cell_t);
extern double phTm, phHm, phTv, phHv, phSe;    

cell_t citer(node_t ln, cell_t oc, node_t rn, double t, double dt){
    assert( ln.r <= rn.r );
    assert( ln.r <= oc.r && oc.r <= rn.r );
    
    cell_t nc;
    
    double 
        r = nc.r = oc.r,
        h = rn.r - ln.r,
        dH = 0;
    
    nc.U =  oc.U  + dt*((ln.W*ln.r - rn.W*rn.r)/(h*r) + phUs(t,oc) - phUl(oc));
    if(!IS_NAT(nc.U)){
        printf("\nt=%f r=%f: non-natural U: old=%e new=%e\n",t,r,oc.U,nc.U);
        exit(1);
    }
    
    nc.Tl = oc.Tl + dt*((ln.K*ln.r - rn.K*rn.r)/(h*r) + phUl(oc) + LATTICE_SOURCE)/phCl(oc);
    if(!IS_NAT(nc.Tl)){
        printf("\nt=%f r=%f: non-natural Tl: old=%e new=%e\n",t,r,oc.Tl,nc.Tl);
        exit(1);
    }
    
    nc.N =  oc.N  + dt*((ln.J*ln.r - rn.J*rn.r)/(h*r) + phGe(t,oc));
    if(!IS_NAT(nc.N)){
        printf("\nt=%f r=%f: non-natural N: old=%e new=%e\n",t,r,oc.N,nc.N);
        exit(1);
    }
    
    nc.Te = (nc.U/nc.N - phEg(nc))/(3*PH_BLTZ);
    if(!IS_NAT(nc.Te)){
        printf("\nt=%f r=%f: non-natural Te: old=%e new=%e\n",t,r,oc.Te,nc.Te);
        exit(1);
    }
    
    dH = phCl(nc)*(nc.Tl - phTm);
    nc.Hm = oc.Hm + dH;
    if(dH >= 0.0) nc.Tl = (nc.Hm <= phHm)? phTm : nc.Tl;
    nc.Hm = (nc.Hm < 0)? 0: (nc.Hm > phHm) ? phHm : nc.Hm;

    dH = phCl(nc)*(nc.Tl - phTv);
    nc.Hv = oc.Hv + dH;
    if(dH >= 0.0) nc.Tl = (nc.Hv <= phTv)? phTv : nc.Tl;
    nc.Hv = (nc.Hv < 0)? 0: (nc.Hv > phTv) ? phTv : nc.Hv;
    
    return nc;
}

void niter( node_t* n, cell_t lc, cell_t rc ){
    assert( lc.r <= rc.r );
    assert( lc.r <= n->r && n->r <= rc.r );
    
    
    double
        h = rc.r - lc.r,
        hl= n->r - lc.r,
        hr= rc.r - n->r;
    n->J = - FAV(phDf)*(DC(N) +
                        (( IS_REAL(CAV(N)/CAV(Te)) )? ( 2*CAV(N)*DF(phEg)/(PH_BLTZ*CAV(Te)) + CAV(N)*DC(Te)/(2*CAV(Te)) ) : 0));
    assert(IS_REAL(n->J));
    n->W = ( FAV(phEg) + 3*PH_BLTZ*CAV(Te) )*n->J - FAV(phKe)*DC(Te);
    assert(IS_REAL(n->W));
    n->K = - FAV(phKl)*DC(Tl);
    assert(IS_REAL(n->K));    
}
