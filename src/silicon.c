#include <math.h>
#include <assert.h>
#include "inc/typedef.h"
#include "inc/ph_constants.h"

double
// ======= Atomic constants ====================== //         
        phM = 26.4e9,
        phZ = 14,
// ======= Melting and Varourization constants === //
        phTm = 1683.0,   // K
        phTv = 2953.0,   // K
         
        phHm = 28.04,    // eV/nm^3
        phHv = 214.1,    // eV/nm^3
         
// ======== El. Term. Conductivity =============== //
        phMe = 140.0,    // nm^2/eV fs
        phMh = 45.0,     // nm^2/eV fs
          
         
// ======= Miscellaneous constants =============== //
        phTep = 300,     // fs
         
        phSe = 70000,     // eV/nm
        phTed = 1.0,      // fs
        phRd = 2.5,       // nm
        phN = 98,	   //  nm-3
        phSig = 1.0;      // fs
         

double phDf(cell_t cell){
    return 1.8*300/cell.Tl;
}
double phUs( double t, cell_t cell){
    /*
          * Us = AD(r) b exp(-bt),
          * 
          * D - Gaussian with the only sigma parameter
          * A - is a constant
          * b = 1/Tau, where Tau is energy deposition time equals to 1 fs (in paper)
          * rmax = 5*sigma (in paper)
          * 
          */

    double A, D, s;
    int d;
    double r = cell.r;
         
    s = 0.65; // nm
    d = 5;    // rmax = d*sigma
         
    A = phSe/( (2*M_PI*s*s) * (1 - exp(-d*d/2.0)) );
    D = exp(-r*r/(2*s*s));
         
    return A*D*exp( -t/ phTed )/phTed;
}

double phUl( cell_t cell ){
    double Tl = cell.Tl,
            Te = cell.Te,
            N  = cell.N;
    return 3*N*PH_BLTZ*(Te - Tl)/phTep;
}

double phEg( cell_t cell ){
    double   Tl = cell.Tl,
            N  = cell.N;
                  
    // 1.16 - 7.02e-4*Tl**2/(Tl + 1108) - 1.5e-8*N**(1/3)
    return 1.16; // - 7.02e-4*Tl*Tl/(Tl + 1108) - 1.5e-8*pow(N,1.0/3);
}

// ======= Lattice heat func [eV K-1 nm-3 ] ====== //
double phCl( cell_t cell){
    double Tl = cell.Tl;
         
    double A, B, C, D;
         
    if(60.0 <= Tl && Tl < 300.0){
        A = -0.1354;
        B = 4.486e-3;
        C = -5.207e-6;
        D = 2.32;
    }
    else if(300.0 <= Tl && Tl <= phTm){
        A = 0.7007;
        B = 1.469e-4;
        C = 3.183e-8;
        D = 2.32;
    }
    else if(Tl > phTm){
        A = 1.045;
        B = C = 0;
        D = 2.50;
    }
    else{
        assert(0);
        return 0.0;
    }
         
    return D*(A+B*Tl+C*Tl*Tl)*6.242e-3;
}
         
// Conductivity eV fs(-1) nm(-1) K(-1)
double phKl( cell_t cell){
    double Tl = cell.Tl;
    if(60.0 <= Tl && Tl <= phTm)
        return 6.242e-4*1042.0*pow( Tl , -1.158 );
    else if(phTm <= Tl && Tl <= phTv)
        return 6.242e-4*0.14;
    else if(Tl > phTv)
        return 6.242e-4*8.76e-5*sqrt( Tl );
    else{
        assert(0);
        return 0.0;
    }
}

// Thermal electron-holes conductivity eV fs(-1) nm(-1) K(-1)
double phKe( cell_t cell){
    double   Tl = cell.Tl,
            N  = cell.N;
    double p = 1.12;
    return (2.5-p)*(phMe+phMh)*pow(PH_BLTZ,2.0)*Tl*N/2;
}  

double phGe( double t, cell_t cell){
    double Te = cell.Te, N=cell.N;
    double   g = 1.0/3.0*phUs(t,cell)/(phEg(cell)+3*PH_BLTZ*Te),
            r = (N==0)? 0:( 3.8e-4*N*N*N - 3.6e-5*N*exp(-phEg(cell)/(1.5*PH_BLTZ*Te)));
    return g;
//                - r;

}
